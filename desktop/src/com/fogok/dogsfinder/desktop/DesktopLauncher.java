package com.fogok.dogsfinder.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.fogok.dogsfinder.Main;
import com.fogok.dogsfinder.NativeController;

import java.awt.GraphicsEnvironment;

public class DesktopLauncher implements NativeController {

    private static DesktopLauncher launcher = new DesktopLauncher();

	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

		config.width = 480 / 2;
		config.height = 854 / 2;

        config.width = 480 ;
        config.height = 854 ;


        config.width = 384 ;
        config.height = 683 ;

//		config.x = - GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode().getWidth() + config.width + 20;
		config.x = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode().getWidth() - config.width - 20;
		config.y = 20;

		new LwjglApplication(new Main(launcher), config);
	}

    @Override
    public void signInToGoogle() {

    }
}
