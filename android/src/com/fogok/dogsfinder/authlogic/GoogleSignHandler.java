package com.fogok.dogsfinder.authlogic;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.fogok.dogsfinder.AndroidLauncher;
import com.fogok.dogsfinder.R;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

public class GoogleSignHandler {

    public static final String TAG = "ServerAuthCodeActivity";
    public static final int RC_GET_AUTH_CODE = 9003;

    private GoogleSignInClient mGoogleSignInClient;
    private AndroidLauncher mainActivity;

    /***
     * TODO: Сделать авторизацию как здесь короче https://developers.google.com/identity/sign-in/android/additional-scopes
     * @param mainActivity
     */


    public GoogleSignHandler(AndroidLauncher mainActivity) {
        this.mainActivity = mainActivity;

        // [START configure_signin]
        // Configure sign-in to request offline access to the user's ID, basic
        // profile, and Google Drive. The first time you request a code you will
        // be able to exchange it for an access token and refresh token, which
        // you should store. In subsequent calls, the code will only result in
        // an access token. By asking for profile access (through
        // DEFAULT_SIGN_IN) you will also get an ID Token as a result of the
        // code exchange.
        String serverClientId = mainActivity.getString(R.string.server_client_id);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(serverClientId)
                .requestEmail()
                .build();
//        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
//                .requestScopes(new Scope(Scopes.PROFILE))
//                .requestServerAuthCode(serverClientId)
//                .requestEmail()
//                .build();
        // [END configure_signin]

        mGoogleSignInClient = GoogleSignIn.getClient(mainActivity, gso);

    }

    public void checkLoginedUser(){
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(mainActivity);
        //TODO: update
    }



    public void startSignIntentAndCatchAuthCodeAsync(){
        // Start the retrieval process for a server auth code.  If requested, ask for a refresh
        // token.  Otherwise, only get an access token if a refresh token has been previously
        // retrieved.  Getting a new access token for an existing grant does not require
        // user consent.

        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        mainActivity.startActivityForResult(signInIntent, RC_GET_AUTH_CODE);
    }

    public void catchAuthCode(Intent data){
        Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
        try {
            GoogleSignInAccount account = task.getResult(ApiException.class);
            String authCode = account.getServerAuthCode();


            Toast toast = Toast.makeText(mainActivity.getApplicationContext(),
                    String.format("Sign-in compltetd %s", account.getEmail()), Toast.LENGTH_SHORT);
            toast.show();
            // Show signed-un UI
//            updateUI(account);

            // TODO(developer): send code to server and exchange for access/refresh/ID tokens
        } catch (ApiException e) {
            Toast toast = Toast.makeText(mainActivity.getApplicationContext(),
                    String.format("Sign-in failed %s", e), Toast.LENGTH_SHORT);
            toast.show();
//            updateUI(null);
        }
    }

    public void signOut() {
        mGoogleSignInClient.signOut().addOnCompleteListener(mainActivity, new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
//                updateUI(null);
            }
        });
    }

    public void revokeAccess() {
        mGoogleSignInClient.revokeAccess().addOnCompleteListener(mainActivity,
                new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
//                        updateUI(null);
                    }
                });
    }


}
