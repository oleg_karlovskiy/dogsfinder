package com.fogok.dogsfinder;

import android.content.Intent;
import android.os.Bundle;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.fogok.dogsfinder.authlogic.GoogleSignHandler;

public class AndroidLauncher extends AndroidApplication implements NativeController {

    private GoogleSignHandler googleSignHandler;

	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        googleSignHandler = new GoogleSignHandler(this);
		initialize(new Main(this), config);
	}

    @Override
    public void signInToGoogle() {
        googleSignHandler.startSignIntentAndCatchAuthCodeAsync();
    }

    @Override
    protected void onStart() {
        super.onStart();
        googleSignHandler.checkLoginedUser();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case GoogleSignHandler.RC_GET_AUTH_CODE:
                googleSignHandler.catchAuthCode(data);
                break;
        }
    }

}
