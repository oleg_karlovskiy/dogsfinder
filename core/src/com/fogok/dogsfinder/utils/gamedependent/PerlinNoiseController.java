package com.fogok.dogsfinder.utils.gamedependent;

import com.fogok.dogsfinder.utils.ImprovedNoise;

public class PerlinNoiseController {

    private float[] curvedLineData = new float[30000];
    public final static float DOT_DISTANCE = 0.08f;

    public PerlinNoiseController() {
        //init curvedLine
        generateNoise();
    }

    private void generateNoise(){
        double x = 0;
        double y = 0;
        double z = 0;
        for (int i = 0; i < curvedLineData.length; i++) {
            x += 0.01d;

            curvedLineData[i] = (float)ImprovedNoise.noise(x, y, z);
        }
    }

    public float[] getCurvedLineData() {
        return curvedLineData;
    }
}
