package com.fogok.dogsfinder.utils;


import com.fogok.dogsfinder.Main;

/**
 * Created by FOGOK on 07.01.2017 15:22.
 * Если ты это читаешь, то знай, что этот код хуже
 * кожи разлагающегося бомжа лежащего на гнилой
 * лавочке возле остановки автобуса номер 985
 */
public class Timer {

    private float seconds, targetSeconds;
    private boolean isStarted;
    public Timer(float targetSeconds) {
        this.targetSeconds = targetSeconds;
        seconds = 0f;
        isStarted = false;
    }

    public boolean next(boolean isNeedMdt){
        isStarted = true;
        seconds += isNeedMdt ? 0.016f * Main.mdT : 0.016f;
        return seconds > targetSeconds;
    }

    public boolean next(){
        return next(true);
    }

    public void setSeconds(float seconds) {
        if (seconds <= targetSeconds)
        this.seconds = seconds;
    }

    public float getProgress(){
        return GMUtils.normalizeOneZero(seconds / targetSeconds);
    }

    public void reset(float newTargetSeconds){
        targetSeconds = newTargetSeconds;
        reset();
    }

    public void reset(){
        seconds = 0f;
        isStarted = false;
    }

    public boolean isEnd(){
        return seconds > targetSeconds;
    }

    public boolean isStarted() {
        return isStarted;
    }
}
