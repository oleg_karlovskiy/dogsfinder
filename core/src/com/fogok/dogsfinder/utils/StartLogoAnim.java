package com.fogok.dogsfinder.utils;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.fogok.dogsfinder.Main;
import com.fogok.dogsfinder.utils.guiwidgets.ARHelper;
import com.fogok.dogsfinder.utils.guiwidgets.BaseWidget;
import com.fogok.dogsfinder.utils.guiwidgets.LetterEmersionTextLabel;

/**
 * Created by FOGOK on 16.06.2017 15:24.
 */

public class StartLogoAnim {

    private LetterEmersionTextLabel textLabel;
    private Sprite logoFogok;
    private boolean isCompleteStartLogo;

    private float animSpeed = 0.002f;

    private int animLogoIters, animLogoMax = 1;

    private Timer timer;

    public StartLogoAnim(TGen tGen) {
        logoFogok = tGen.getNewSprite(0, TGen.AtlasType.MISC);
        ARHelper.setSpriteSize(logoFogok, Main.HEIGHT / 6f, false);
        logoFogok.setCenter(Main.WIDTH / 2f, Main.HEIGHT / 2f);
        logoFogok.setOriginCenter();

        textLabel = new LetterEmersionTextLabel(Main.WIDTH / 2f, 0f, 0.6f,
                "fogok production", 0.1f);

        textLabel.setColor(Color.BLACK);

        timer = new Timer(3f);
    }


    public void draw(SpriteBatch batch) {

        float alpha = 1f;
        if (timer.getProgress() > 0.8f)
            alpha = GMUtils.normalizeOneZero((1f - timer.getProgress()) / 0.2f);
        else if (timer.getProgress() < 0.5f)
            alpha = GMUtils.normalizeOneZero(timer.getProgress() / 0.5f);

        switch (animLogoIters) {
            case 0:
                logoFogok.scale(animSpeed * Main.mdT);
                logoFogok.setAlpha(alpha);
                logoFogok.draw(batch);

                textLabel.setPosition(BaseWidget.Align.LeftBottom, textLabel.getBounds().getX(),  logoFogok.getY() -
                        textLabel.getBounds().getHeight() - 0.2f -
                        logoFogok.getHeight() * (logoFogok.getScaleY() - 1f));
                break;
        }

        textLabel.setSizeText(textLabel.getSizeText() + animSpeed * Main.mdT * 0.7f);
        textLabel.setAlpha(alpha);
        textLabel.draw(batch);

        if (timer.next()) {
            if (animLogoIters < animLogoMax) {
                switch (animLogoIters) {
                    case 0:
                        textLabel.setCharsSpeed(0.07f);
                        textLabel.resetAnimation();
                        textLabel.setSizeText(0.8f);
                        textLabel.setText("представляет");
                        textLabel.setPosition(BaseWidget.Align.LeftBottom, Main.WIDTH / 2f, Main.HEIGHT / 2f);
                        break;
                }
                animLogoIters++;
                timer.reset(2.3f);
            }else
                isCompleteStartLogo = true;
        }
    }

    public void setCompleteStartLogo(boolean completeStartLogo) {
        isCompleteStartLogo = completeStartLogo;
    }

    public boolean isCompleteStartLogo() {
        return isCompleteStartLogo;
    }
}
