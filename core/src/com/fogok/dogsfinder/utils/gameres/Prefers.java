package com.fogok.dogsfinder.utils.gameres;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

/**
 * Created by FOGOK on 25.10.2016 7:58.
 * Если ты это читаешь, то знай, что этот код хуже
 * кожи разлагающегося бомжа лежащего на гнилой
 * лавочке возле остановки автобуса номер 985
 */
public class Prefers {

    private static Preferences prefs;

    /////// DO NOT CHANGE FINAL STRINGS

    public static final String IsFirstStart = "IsFirstStart";
    public static final String KeyAppLaunchs = "KeyAppLaunchs";


    /////// DO NOT CHANGE FINAL STRINGS

    public static void initPrefs(){
        prefs = Gdx.app.getPreferences("My Preferences");
    }

    public static void putInt(String key, int whoEdit){
        prefs.putInteger(key, whoEdit);
        prefs.flush();
    }

    public static void putFloat(String key, float whoEdit){
        prefs.putFloat(key, whoEdit);
        prefs.flush();
    }

    public static float getFloat(String key){
        return prefs.getFloat(key, 1f);
    }

    public static int getInt(String key){
        return prefs.getInteger(key, 0);
    }

    public static void putBool(String key, boolean whoEdit){
        prefs.putBoolean(key, whoEdit);
        prefs.flush();
    }

    public static boolean getBool(String key, boolean defVal){
        return prefs.getBoolean(key, defVal);
    }

//    public static void setStarVars0(){
//        for (int i = 0; i < 6; i++) {
//            char[] chars = new char[27];
//            for (int i2 = 0; i2 < chars.length; i2++) {
//                chars[i2] = '0'; //// i2 < 12 ? '3' : '0' noInPROD!!
//            }
//            prefs.putString(KeyStarsLevelsInC1_6 + (i + 1), new String(chars));
//            prefs.putInteger(Prefers.KeyOpenedLevelsInC1_6 + (i + 1), 1);
//        }
//        prefs.putInteger(Prefers.KeyOpenedContinents, 1);
//        prefs.flush();
//    }

    public static String getString(String key) {
        return prefs.getString(key);
    }
//
//    public static boolean isContinentOpen(int continent) {   //все параметры с 0 начинаются (level, continent)
//        return prefs.getInteger(KeyOpenedContinents, 1) > continent;
//    }
//
//    public static boolean isLevelOpen(int continent, int level) {   //все параметры с 0 начинаются (level, continent)
//        return prefs.getInteger(KeyOpenedLevelsInC1_6 + (continent + 1), 1) > level;
//    }
//
//    public static int getStarCount(int continent, int level) {  //все параметры с 0 начинаются (level, continent)
//        return Character.getNumericValue(prefs.getString(KeyStarsLevelsInC1_6 + (continent + 1)).toCharArray()[level]);
//    }
//
//    public static int getStarCount(int continent) {  //все параметры с 0 начинаются (level, continent)
//        int response = 0;
//        for (int i = 0; i < 27; i++) {
//            response += Character.getNumericValue(prefs.getString(KeyStarsLevelsInC1_6 + (continent + 1)).toCharArray()[i]);
//        }
//        return response;
//    }
//
//    public static void putStars(int continent, int level, int curStar) { //все параметры с 0 начинаются (level, continent)
//        char[] chars = prefs.getString(KeyStarsLevelsInC1_6 + (continent + 1)).toCharArray();
//        if (curStar > Character.getNumericValue(chars[level]))
//            chars[level] = Integer.toString(curStar).charAt(0);
//        prefs.putString(KeyStarsLevelsInC1_6 + (continent + 1), new String(chars));
//        prefs.flush();
//    }
}
