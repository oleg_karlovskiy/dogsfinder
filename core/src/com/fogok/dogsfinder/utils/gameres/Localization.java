package com.fogok.dogsfinder.utils.gameres;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;

/**
 * Created by FOGOK on 24.10.2016 21:48.
 * Если ты это читаешь, то знай, что этот код хуже
 * кожи разлагающегося бомжа лежащего на гнилой
 * лавочке возле остановки автобуса номер 985
 */
public class Localization {

    public enum Lang {
        ENG, RUS
    }
    private static Lang lang = Lang.RUS;

    public enum LettersKey{
        KILLED, UNITTESTINGPERFORMANCE, WIN, LOSE, PAUSE, MAPTEXTLEFT, MAPTEXTRIGHT,
        SELECT_LEVEL, SETTINGS, HELP, SEAHORSE, REDACULA, CUTTLEFISH, TERRIBLELOBSTER,
        COSMOSTAR, GIANTMEDUSA, LANGUAGE, GOODGRAPHICS, YOUSURE, HELP_TEXT,
        MAPWARLEARN1, MAPWARLEARN2, MAPWARLEARN3, MAPWARLEARN4, LEVEL15LEARN1, LEVEL15LEARN2,
        COMINGSOON1, COMINGSOON2, LEVELFIRSTLEARN1, LEVELFIRSTLEARN2, LEVELFIRSTLEARN3, LEVELFIRSTLEARN4,
        BOSSLEARN1, BOSSLEARN2, BOSSLEARN3, BOSSLEARN4, NEWLOCATIONWORM, DIFFICULTY_TITLE,
        DIFF_LOW, DIFF_MEDIUM, DIFF_HARD, SKIN_SELECT_TITLE, VIEW_VIDEO, VIEW_VIDEO_DESK
    }

//    //ENG
//    private static String[] engLetters = new String[]{
//            "KILLED", "UNIT TESTING PERFORMANCE", "WIN", "LOSE", "PAUSE", "EASY", "HARD", "SELECT LEVEL", "SETTINGS",
//            "HELP", "LANGUAGE", "GOOD GRAPHICS", "YOU SURE ?",
//            "YOU NEED TO DESTROY ALL THE EVIL FISH, WHICH\n" +
//                    "TRYING TO KILL THE POOR WORM\n" +
//                    "\n" +
//                    "EVERY WEAPON IS KILLING ONLY ONE KIND OF FISH\n" +
//                    "\n" +
//                    "GAME DEVELOPER:\nOLEG KARLOVSKIY"
//    };
    ///

    public static Lang getCurrentLang(){
        return lang;
    }

    private static JsonValue localizationJson;

    public static void init() {
        JsonReader jsonReader = new JsonReader();
        localizationJson = jsonReader.parse(Gdx.files.internal("text/texts.json").readString());
    }

    public static String getText(LettersKey key){
        switch (lang){
            case RUS:
                return localizationJson.get("rus").getString(key.name());
            case ENG:
                return localizationJson.get("eng").getString(key.name());

            default: return null;
        }
    }

}
