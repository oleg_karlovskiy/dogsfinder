package com.fogok.dogsfinder.utils.guiwidgets;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.fogok.dogsfinder.utils.Timer;


/**
 * Created by FOGOK on 16.06.2017 16:56.
 */

public class LetterEmersionTextLabel extends TextLabel {


    private Timer timer;
    private char[] textChars;
    private StringBuilder charSettingsStrBld, finalStringStrBld;
    private int currentCharAnimate = 0;
    private float charsSpeed;


    private static String openCl = "[", closeCl = "]", hashCl = "#";

    public LetterEmersionTextLabel(float x, float y, float sizeText, String text, float charsSpeed) {
        super(x, y, sizeText, text);
        this.charsSpeed = charsSpeed;
        timer = new Timer(charsSpeed);
    }

    public void setCharsSpeed(float charsSpeed) {
        this.charsSpeed = charsSpeed;
    }

    @Override
    public void setText(String text) {
        super.setText(text);
        charSettingsStrBld = new StringBuilder();
        finalStringStrBld = new StringBuilder();
        textChars = text.toCharArray();
    }

    public void resetAnimation(){
        currentCharAnimate = 0;
        timer.reset(charsSpeed);
    }

    public boolean isAnimEnd(){
        return currentCharAnimate == textChars.length && timer.isEnd();
    }

    public void draw(SpriteBatch batch, boolean isAnimate) {
        if (isAnimate)
            animate();
        super.draw(batch);
    }

    @Override
    public void draw(SpriteBatch batch) {
        draw(batch, true);
    }

    private void animate(){
        if (timer.next()) {
            if (currentCharAnimate < textChars.length) {
                currentCharAnimate++;
                timer.reset();
            }
        }
        dirty = true;
    }

    @Override
    void refreshTextParams() {

        finalStringStrBld.setLength(0);
        for (int i = 0; i < textChars.length; i++) {

            if (textChars[i] != ' ') {
                charSettingsStrBld.setLength(0);
                charSettingsStrBld.append(openCl);
                charSettingsStrBld.append(hashCl);
                if (i >= currentCharAnimate)
                    color.a = i > currentCharAnimate ? 0f : timer.getProgress() * alpha;
                else
                    color.a = alpha;

                charSettingsStrBld.append(color.toString());
                color.a = 1f;
                charSettingsStrBld.append(closeCl);
                charSettingsStrBld.append(textChars[i]);
                finalStringStrBld.append(charSettingsStrBld.toString());
            } else {
                finalStringStrBld.append(' ');
                finalStringStrBld.append(' ');//noBag
            }


        }

        text = finalStringStrBld.toString();

        super.refreshTextParams();
    }
}
