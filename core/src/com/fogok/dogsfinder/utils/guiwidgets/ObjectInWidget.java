package com.fogok.dogsfinder.utils.guiwidgets;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

/**
 * Created by FOGOK on 20.08.2017 6:24.
 */

public interface ObjectInWidget {
    Rectangle getBoundsObjectToWidget(); //должен давать ссылку на bounds, по которым будет отрисовываться объект, т.к. этими bounds мы будем управлять
    void drawToWidget(SpriteBatch batch, float scale);
    void selectElement();
}
