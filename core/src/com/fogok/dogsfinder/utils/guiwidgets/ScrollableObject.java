package com.fogok.dogsfinder.utils.guiwidgets;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Rectangle;
import com.fogok.dogsfinder.Main;
import com.fogok.dogsfinder.utils.FloatAnimator;
import com.fogok.dogsfinder.utils.GMUtils;

/**
 * Created by FOGOK on 20.08.2017 17:41.
 */

public abstract class ScrollableObject extends BaseWidget{

    private final static float TILT_GRAPH_INTERPOLATION_CFF = 0.4f;     //чтобы у интерполяции pow3out не было слишком быстрого увеличения значения в начале, нам необходимо немного наклонить график функции интерполяции, для этого мы сделаем изменение T медленнее чуть более чем в 2 раза

    private final static float RIBBON_OFFSET = 1.5f; //насколько будет уезжать ещё наш график (аккуратно меняем, оно связано с переменной выше, и работает немного магисеским образом :)
    private float AUTOSTOP_RANGE;

    private float wrapperScrollPositon; //текущая позиция скрула, является прослойкой, для реализации резиновых краёв
    float scrollPosition;

    private float firstTouchPosition; //позиция при первом таче, нужна для расчёта разницы, насколько сдвинули точку касания (палец, мышь ?)
    private float fadeX, fadeY, fadeSpeed;
    private float endPoint;     //крайняя точка

    private float autoStopPoints[];

    float boundMin, boundMax; //ограничения

    private boolean isAutoStopEnabled;
    private boolean startStopProcedure;
    private boolean isStopProcedureCompleted;
    float scaleStopCffSelected; //используем эту переменную для текущего выбранного объекта, чтобы его там увеличить или как-то выделить //return value: 0 - 1f
    int objectIndexSelected;
    private FloatAnimator stopProcedureAnimator;

    private boolean horisontalVertical;

    private boolean lastIsTouched;

    private boolean previewProcedure;
    private float startPreviewSpeed;

    ScrollableObject(Rectangle bounds, Align align, boolean horisontalVertical, float endPoint) {
        super(bounds, align);
        this.horisontalVertical = horisontalVertical;
        this.endPoint = endPoint;
    }

    void setAutoStopPoints(float pointsOffset, int countObjects) {
        autoStopPoints = new float[countObjects];
        for (int i = 0; i < autoStopPoints.length; i++) {
            autoStopPoints[i] = -pointsOffset * i;
        }

        AUTOSTOP_RANGE = pointsOffset / 2f;
        isAutoStopEnabled = true;
        stopProcedureAnimator = new FloatAnimator(0f, 0f, 0f, Interpolation.fade);
    }

    private void setScrollPosition(boolean refreshFade) {    //выставляем scrollPosition в зависимости от того, переходим ли мы границу или нет TODO: refactor this BEAUTIFUL
        if (wrapperScrollPositon <= boundMax && wrapperScrollPositon >= boundMin || !refreshFade) {
            scrollPosition = wrapperScrollPositon;
        } else {
            if (wrapperScrollPositon > boundMax)
                scrollPosition = Interpolation.pow3Out.apply(boundMax, boundMax + RIBBON_OFFSET, GMUtils.normalizeOneZero((wrapperScrollPositon - boundMax) * TILT_GRAPH_INTERPOLATION_CFF / 1.5f));
            else
                scrollPosition = Interpolation.pow3Out.apply(boundMin, boundMin - RIBBON_OFFSET, GMUtils.normalizeOneZero(Math.abs(wrapperScrollPositon - boundMin) * TILT_GRAPH_INTERPOLATION_CFF / 1.5f));
        }
    }

    void handleScroll(){    //TODO: noMultiTouch, need it ?
//        Main.DEBUG_VALUE1 = "scrollPosition: " + scrollPosition + "; wrapperScrollPosition: " + wrapperScrollPositon;

        //позиции x и y касания
        float touchPositionX = (Main.WIDTH / Gdx.graphics.getWidth()) * Gdx.input.getX(), touchPositionY = Main.HEIGHT / Gdx.graphics.getHeight() * (Gdx.graphics.getHeight() - Gdx.input.getY());    //TODO: use camera.unproject
        boundMin = -endPoint; boundMax = 0f;


        if (Gdx.input.justTouched()) {
            firstTouchPosition = (horisontalVertical ? touchPositionX : touchPositionY) - wrapperScrollPositon;
        }


        if (Gdx.input.isTouched() && !previewProcedure) {
            lastIsTouched = true;
            if (bounds.contains(touchPositionX, touchPositionY)) {
                if (horisontalVertical) {
                    //horizontal...
                    wrapperScrollPositon = touchPositionX - firstTouchPosition;
                    fadeX = Gdx.input.getDeltaX() * (Main.WIDTH / Gdx.graphics.getWidth());
                    setScrollPosition(true);


                } else {
                    //vertical...
                    //TODO: need end it
                }
            }
        } else {
            if (horisontalVertical) {

                if (lastIsTouched) {
                    isStopProcedureCompleted = startStopProcedure = lastIsTouched = false;

                    wrapperScrollPositon = scrollPosition;
                    if (wrapperScrollPositon > boundMax)
                        fadeSpeed = getBInGeometricalProgress(Math.abs(wrapperScrollPositon - boundMax), 0.8f) * 0.8f;
                    else if (wrapperScrollPositon < boundMin)
                        fadeSpeed = getBInGeometricalProgress(Math.abs(boundMin - wrapperScrollPositon), 0.8f) * 0.8f;
                }

                if (!previewProcedure)
                    fadeX *= 0.8f;
                else{
                    fadeX = startPreviewSpeed * (wrapperScrollPositon / -endPoint) + 0.15f;
                    if (fadeX < 0.01f) {
                        previewProcedure = false;
                        fadeX = 0f;
                        lastIsTouched = true;
                    }
                }

                if (!startStopProcedure && !previewProcedure) {
                    if (wrapperScrollPositon > boundMax)
                        fadeX = -fadeSpeed;
                    else if (wrapperScrollPositon < boundMin)
                        fadeX = fadeSpeed;
                }


                wrapperScrollPositon += fadeX * (Gdx.graphics.getDeltaTime() / 0.016f);
                setScrollPosition(false);


                if (isAutoStopEnabled) {

                    if (Math.abs(fadeX) < 0.05f && wrapperScrollPositon < boundMax && wrapperScrollPositon > boundMin) {
                        if (!startStopProcedure && !isStopProcedureCompleted) {
                            for (int i = 0; i < autoStopPoints.length; i++) {
                                if (Math.abs(autoStopPoints[i] - wrapperScrollPositon) < AUTOSTOP_RANGE) {
                                    stopProcedureAnimator.setFrom(wrapperScrollPositon).setTo(autoStopPoints[i]).setAnimationTime(0.3f).resetTime();
                                    startStopProcedure = true;
                                    fadeX = 0f;
                                    break;
                                }
                            }
                        } else if (startStopProcedure) {
                            stopProcedureAnimator.update(Gdx.graphics.getDeltaTime());
                            wrapperScrollPositon = stopProcedureAnimator.current;
                            scrollPosition = wrapperScrollPositon;
                            if (!stopProcedureAnimator.isNeedToUpdate()) {
                                startStopProcedure = false;
                                isStopProcedureCompleted = true;
                            }
                        }
                    }
                }









            } else {
                //TODO: need finish it
            }
        }

        if (horisontalVertical)
            for (int i = 0; i < autoStopPoints.length; i++) {
                if (Math.abs(autoStopPoints[i] - wrapperScrollPositon) < AUTOSTOP_RANGE) {
                    objectIndexSelected = i;
                    if (lastObjectIndexSelected != objectIndexSelected) {
                        lastObjectIndexSelected = objectIndexSelected;
                        selectElement(objectIndexSelected);
                    }
                    scaleStopCffSelected = 1f - GMUtils.normalizeOneZero(Math.abs(autoStopPoints[i] - wrapperScrollPositon) / AUTOSTOP_RANGE);
                    break;
                }
            }

        Main.DEBUG_VALUE2 = "startStopProcedure: " + startStopProcedure + "; \n" + stopProcedureAnimator.toString() + "\nfadeX: " + fadeX + "\nscaleStopCffSelected: " + scaleStopCffSelected + "\nwrapperScrollPositon: " + wrapperScrollPositon;
    }

    int lastObjectIndexSelected;

    protected abstract void selectElement(int i);

    public void startPreviewProcedure(float speed) {
        previewProcedure = true;
        wrapperScrollPositon = -endPoint;

        startPreviewSpeed = speed;
        fadeX = speed;
    }
    private float getBInGeometricalProgress(float sum, float q) {
        return sum - q * sum;
    }



}
