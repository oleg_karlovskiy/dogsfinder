package com.fogok.dogsfinder.utils.guiwidgets;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by FOGOK on 16.06.2017 16:21.
 */

public interface Drawable {
    void draw(SpriteBatch batch);
}
