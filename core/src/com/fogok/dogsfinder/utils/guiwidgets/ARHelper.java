package com.fogok.dogsfinder.utils.guiwidgets;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.fogok.dogsfinder.Main;

/**
 * Created by FOGOK on 16.06.2017 16:05.
 */

public class ARHelper {

    public static float width, height;

                                                                            //value - width/height, true - width, false - height
    public static void setSpriteSize(Sprite sprite, float targetValue, boolean widthHeight){
        if (widthHeight) {
            width = targetValue;
            height = sprite.getHeight() / sprite.getWidth() * width;
        } else {
            height = targetValue;
            width = sprite.getWidth() / sprite.getHeight() * height;
        }
        sprite.setSize(width, height);
    }

    public static <T extends Actor> Cell<T> addToTableWithCustomSize(Table table, Actor widget, float targetValueProc, boolean widthHeight){
        float targetValue = targetValueProc
                / (widthHeight ? Main.WIDTH : Main.HEIGHT)
                * (widthHeight ? Gdx.graphics.getWidth() : Gdx.graphics.getHeight());
        if (widthHeight) {
            width = targetValue;
            height = widget.getHeight() / widget.getWidth() * width;
        } else {
            height = targetValue;
            width = widget.getWidth() / widget.getHeight() * height;
        }
        return (Cell<T>) table.add(widget).width(width).height(height);
    }

    public static void setWidgetSize(Actor widget, float targetValueProc, boolean widthHeight){
        float targetValue = targetValueProc
                / (widthHeight ? Main.WIDTH : Main.HEIGHT)
                * (widthHeight ? Gdx.graphics.getWidth() : Gdx.graphics.getHeight());
        if (widthHeight) {
            width = targetValue;
            height = widget.getHeight() / widget.getWidth() * width;
        } else {
            height = targetValue;
            width = widget.getWidth() / widget.getHeight() * height;
        }
        widget.setSize(width, height);
    }

    public static float getRealWidth(float value){
        return value / Main.WIDTH * Gdx.graphics.getWidth();
    }

    public static float getRealHeight(float value){
        return value / Main.HEIGHT * Gdx.graphics.getHeight();
    }


    public static float getWidth() {
        return width;
    }

    public static float getHeight() {
        return height;
    }
}
