package com.fogok.dogsfinder.utils.guiwidgets;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.fogok.dogsfinder.utils.guiwidgets.listeners.SelectItemListener;


/**
 * Created by FOGOK on 20.08.2017 6:21.
 */

public class LeftRightSlider<T extends ObjectInWidget> extends ScrollableObject {

    private SelectItemListener selectItemListener;
    public void setSelectItemListener(SelectItemListener selectItemListener) {
        this.selectItemListener = selectItemListener;
    }

    private T[] objectsInWidget;
    private Rectangle[] boundsObjects; //управляем объектами исключительно через вот эти ректенглы
    private final float objectOffsets;

    public LeftRightSlider(Rectangle bounds, Align align, T[] objectsInWidget, float objectOffsets) {
        super(bounds, align, true, objectOffsets * (objectsInWidget.length - 1));
        this.objectsInWidget = objectsInWidget;
        this.objectOffsets = objectOffsets;
        setAutoStopPoints(objectOffsets, objectsInWidget.length);
        boundsObjects = new Rectangle[objectsInWidget.length];
        for (int i = 0; i < objectsInWidget.length; i++) {
            T objInWG = objectsInWidget[i];
            boundsObjects[i] = objInWG.getBoundsObjectToWidget();
        }
    }

    public void draw(SpriteBatch batch){
        handleScroll();
        for (int i = 0; i < boundsObjects.length; i++) {
            Rectangle rec = boundsObjects[i];
            rec.setPosition(bounds.x + bounds.getWidth() / 2f - rec.getWidth() / 2f + scrollPosition + objectOffsets * i, bounds.y + bounds.getHeight() / 2f - rec.getHeight() / 2f); //выставляем Y всех объектов в центр ограничивающей области
            objectsInWidget[i].drawToWidget(batch, i == objectIndexSelected ? 1f + scaleStopCffSelected : 1f);
        }
    }

    @Override
    protected void selectElement(int i) {
        objectsInWidget[i].selectElement();
        selectItemListener.selectItem(i);
    }

    public int getSelectedItem(){
        return objectIndexSelected;
    }
}
