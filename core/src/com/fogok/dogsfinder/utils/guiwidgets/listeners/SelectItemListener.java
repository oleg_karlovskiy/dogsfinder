package com.fogok.dogsfinder.utils.guiwidgets.listeners;

/**
 * Created by FOGOK on 8/26/2017 7:19 PM.
 */

public interface SelectItemListener {
    void selectItem(int i);
}
