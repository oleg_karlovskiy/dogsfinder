package com.fogok.dogsfinder.utils;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by FOGOK on 16.07.2016 21:18.
 * Если ты это читаешь, то знай, что этот код хуже
 * кожи разлагающегося бомжа лежащего на гнилой
 * лавочке возле остановки автобуса номер 985
 */
public class TGen {

    public enum AtlasType{
        BACKS, MISC, DOGS, ITEMS
    }

    private TextureAtlas backsTextureAtlas, miscTextureAtlas, dogsTextureAtlas, itemsTextureAtlas;

    public TGen() {
        backsTextureAtlas = new TextureAtlas("graphics/backs.atlas");
        miscTextureAtlas = new TextureAtlas("graphics/misc.atlas");
        dogsTextureAtlas = new TextureAtlas("graphics/dogs.atlas");
        itemsTextureAtlas = new TextureAtlas("graphics/items.atlas");
    }

    public Sprite getNewSprite(int i, AtlasType type){
        return new Sprite(findRegion(i, type));
    }

    public TextureRegion getTR(int i, AtlasType type){
        return new TextureRegion(findRegion(i, type));
    }

    public TextureRegion findRegion(int i, AtlasType type){
        TextureAtlas textureAtlas;
        switch (type) {
            case MISC:
                textureAtlas = miscTextureAtlas;
                break;
            case BACKS:
                textureAtlas = backsTextureAtlas;
                break;
            case DOGS:
                textureAtlas = dogsTextureAtlas;
                break;
            case ITEMS:
                textureAtlas = itemsTextureAtlas;
                break;
            default:
                textureAtlas = backsTextureAtlas;
                break;
        }

        return textureAtlas.findRegion(String.valueOf(i));
    }
    public void dispose() {
        backsTextureAtlas.dispose();
    }
}
