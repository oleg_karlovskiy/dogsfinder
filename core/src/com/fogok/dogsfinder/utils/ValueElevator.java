package com.fogok.dogsfinder.utils;

import com.badlogic.gdx.Gdx;

/**
 * Created by FOGOK on 23.07.2017 17:19.
 */

public class ValueElevator {

    private float current, min, max, speed;    //speed 0-1f in %
    public ValueElevator(float min, float max, float speed) {
        this.min = min;
        this.max = max;
        this.speed = (max - min) * speed;
        current = 0f;
    }

    public float handle(boolean elevate) {
        if (elevate) {
            if (current + speed < max)
                current += speed * (Gdx.graphics.getDeltaTime() / 0.016f);
            else
                current = max;
        } else {
            if (current - speed > min)
                current -= speed * (Gdx.graphics.getDeltaTime() / 0.016f);
            else
                current = min;
        }
        return current;
    }

    public boolean isActive() {
        return current != 0f;
    }
}
