package com.fogok.dogsfinder.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.utils.Array;
import com.fogok.dogsfinder.Main;
import com.fogok.dogsfinder.overridesceneui.SuperBitmapFont;

public class UI {
    private static int SIZE_CHAR_FONT = 32;
    public static float CONVERT_TO_ABSTRACT;

    private static SuperBitmapFont dynamicFont;
    private static ShaderProgram fontShader;

    public static void initializate(){
        float convertToAE = (Main.HEIGHT / (float) Gdx.graphics.getHeight());
        float cffMax = (float) Gdx.graphics.getHeight() / (float) UI.SIZE_CHAR_FONT;

        CONVERT_TO_ABSTRACT = cffMax * convertToAE / Main.HEIGHT;

        coreSetupFonts();
    }

    private static void coreSetupFonts(){
        Array<TextureRegion> textureRegions = new Array<TextureRegion>(3);
        textureRegions.add(new TextureRegion(new Texture(Gdx.files.internal("font/font.png"))));
        textureRegions.add(new TextureRegion(new Texture(Gdx.files.internal("font/font1.png"))));
        textureRegions.add(new TextureRegion(new Texture(Gdx.files.internal("font/font2.png"))));
        for (TextureRegion textureRegion : textureRegions)
            textureRegion.getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);

        dynamicFont = new SuperBitmapFont(new BitmapFont.BitmapFontData(Gdx.files.internal("font/font.fnt"), false), textureRegions, false);
        fontShader = new ShaderProgram(Gdx.files.internal("shaders/font_shader_v.glsl"), Gdx.files.internal("shaders/font_shader_f.glsl"));
        if (!fontShader.isCompiled()) {
            Gdx.app.error("fontShader", "compilation failed:\n" + fontShader.getLog());
        }
    }
    public static void setCff(float cff){
        dynamicFont.setScale(cff * CONVERT_TO_ABSTRACT, 4);
    }

    public static void setAlpha(final float alpha){
        dynamicFont.setColor(dynamicFont.getColor().r, dynamicFont.getColor().g, dynamicFont.getColor().b, alpha);
    }

    public static SuperBitmapFont getDynamicFont() {
        return dynamicFont;
    }

    public static ShaderProgram getFontShader() {
        return fontShader;
    }

    public static void dispose(){
        dynamicFont.dispose();
        dynamicFont = null;
    }

}
