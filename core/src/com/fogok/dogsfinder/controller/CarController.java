package com.fogok.dogsfinder.controller;

import com.fogok.dogsfinder.utils.gamedependent.PerlinNoiseController;

public class CarController {

    private PerlinNoiseController perlinNoiseController;


    public CarController() {
        perlinNoiseController = new PerlinNoiseController();
    }

    public void handle(){

    }

    public float[] getCurvedLine(){
        return perlinNoiseController.getCurvedLineData();
    }
}
