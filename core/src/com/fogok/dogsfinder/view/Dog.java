package com.fogok.dogsfinder.view;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.fogok.dogsfinder.Main;
import com.fogok.dogsfinder.utils.TGen;

public class Dog {

    private MultiTextureObject head, body;

    public Dog(TGen tGen) {
        head = new MultiTextureObject(tGen, new MultiObjectBuilder() {
            @Override
            public void buildObject(SpriteBatch batch, TGen tGen) {
                Sprite core = tGen.getNewSprite(0, TGen.AtlasType.DOGS);
                core.setColor(Color.FIREBRICK);
                Sprite face = tGen.getNewSprite(1, TGen.AtlasType.DOGS);

                core.draw(batch);
                face.draw(batch);
            }
        }, 304, 304);

        head.getMultiSprite().setSize(5f, 5f);
        head.getMultiSprite().setCenter(Main.WIDTH / 2f, Main.HEIGHT / 2f);

        body = new MultiTextureObject(tGen, new MultiObjectBuilder() {
            @Override
            public void buildObject(SpriteBatch batch, TGen tGen) {
                Sprite core = tGen.getNewSprite(2, TGen.AtlasType.DOGS);
                core.setColor(Color.FIREBRICK);
                Sprite face = tGen.getNewSprite(3, TGen.AtlasType.DOGS);

                core.draw(batch);
                face.draw(batch);
            }
        }, 304, 304);

        body.getMultiSprite().setSize(5f, 5f);
        body.getMultiSprite().setPosition(head.getMultiSprite().getX(), head.getMultiSprite().getY() - body.getMultiSprite().getHeight());
    }

    public void draw(SpriteBatch batch) {
        head.draw(batch);
        body.draw(batch);
    }


    public void dispose(){
        head.dispose();
        body.dispose();
    }
}
