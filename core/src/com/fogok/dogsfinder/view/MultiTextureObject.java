package com.fogok.dogsfinder.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.fogok.dogsfinder.utils.TGen;

class MultiTextureObject {

    private /*static */FrameBuffer fbo;
    private /*static */SpriteBatch frameBufferSpriteBatch;

    private Texture fboTexture;
    private Sprite multiSprite;
    private TGen tGen;

    MultiTextureObject(TGen tGen, MultiObjectBuilder multiObjectBuilder, int width, int height) {
        this.tGen = tGen;

        if (frameBufferSpriteBatch == null)
            frameBufferSpriteBatch = new SpriteBatch();

        if (fbo == null)
            fbo = new FrameBuffer(Pixmap.Format.RGBA8888, width, height, true);

        renderToFrameBuffer(frameBufferSpriteBatch, multiObjectBuilder);
    }

    private void renderToFrameBuffer(SpriteBatch batch, MultiObjectBuilder multiObjectBuilder){
        fbo.begin();

        Gdx.gl.glClearColor(0f, 0f,0f, 0f);
        Gdx.gl.glClear( GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT );

        batch.getProjectionMatrix().setToOrtho2D(0, 0, fbo.getWidth(), fbo.getHeight());
        batch.begin();
        multiObjectBuilder.buildObject(batch, tGen);
        batch.end();
        batch.setBlendFunction(GL20.GL_ONE_MINUS_DST_ALPHA, GL20.GL_SRC_ALPHA);

        fbo.end();
        fboTexture = fbo.getColorBufferTexture();
        fboTexture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        multiSprite = new Sprite(fboTexture);
        multiSprite.setFlip(false, true);
    }


    public void draw(SpriteBatch batch){
        multiSprite.draw(batch);
    }

    public Sprite getMultiSprite() {
        return multiSprite;
    }

    public void dispose(){
        if (frameBufferSpriteBatch != null) {
            frameBufferSpriteBatch.dispose();
            frameBufferSpriteBatch = null;
        }

        if (fbo != null) {
            fbo.dispose();
            fbo = null;
        }
    }

}

interface MultiObjectBuilder{
    void buildObject(SpriteBatch batch, TGen tGen);
}
