package com.fogok.dogsfinder.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.fogok.dogsfinder.Main;
import com.fogok.dogsfinder.utils.ShaderBatch;
import com.fogok.dogsfinder.utils.TGen;

public abstract class BaseScreen implements Screen {

    protected Color color = Color.WHITE;
    protected ShaderBatch batch;
    protected TGen tGen;
    protected Stage stage;
    protected OrthographicCamera camera;
    protected Main main;

    protected boolean renderStage = false;
    protected boolean customRender = false;
    protected boolean renderSprites = true;

    public BaseScreen(Main main) {
        this.batch = main.getBatch();
        this.tGen = main.gettGen();
        this.stage = main.getStage();
        this.camera = main.getCamera();
        this.main = main;
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(color.r, color.g, color.b, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        Gdx.gl.glEnable(GL20.GL_SCISSOR_TEST);
        Gdx.gl.glScissor(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

        if (renderStage){
            stage.act(delta * Main.mdT);
            stage.draw();
        }

        if (renderSprites) {
            camera.update();
            batch.setProjectionMatrix(camera.combined);
            batch.begin();
            renderSpritesInternal(delta * Main.mdT);
            batch.end();
        }

        if (customRender)
            customRender(delta * Main.mdT);

        Gdx.gl.glDisable(GL20.GL_SCISSOR_TEST);
    }

    protected abstract void renderSpritesInternal(float delta);

    protected void customRender(float delta){

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

}
