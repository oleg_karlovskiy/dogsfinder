package com.fogok.dogsfinder.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.fogok.dogsfinder.Main;
import com.fogok.dogsfinder.controller.CarController;
import com.fogok.dogsfinder.utils.gamedependent.PerlinNoiseController;

public class CarGameScreen extends BaseScreen {

    private ShapeRenderer shapeRenderer;
    private CarController carController;

    public CarGameScreen(Main main) {
        super(main);

        renderSprites = true;
        renderStage = false;
        customRender = true;

        shapeRenderer = new ShapeRenderer();
        carController = new CarController();
    }

    private float currentXpos = 0;

    @Override
    protected void customRender(float delta) {

        shapeRenderer.setProjectionMatrix(camera.combined);
        shapeRenderer.setColor(Color.BLACK);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        Gdx.gl.glLineWidth(3f);
//        float currentX = -currentXpos;
//        float lastX = 0f;
////        float lastY = Main.HEIGHT / 2f;
//        float lastY = 0f;
//        float yCenter = Main.HEIGHT / 2f;
//        for (float rawCurrentY : carController.getCurvedLine()){
//            currentX += PerlinNoiseController.DOT_DISTANCE;
//            float currentY = rawCurrentY * 8f + yCenter;
//            shapeRenderer.line(lastX, lastY, currentX, currentY, Color.GREEN, Color.GREEN);
//            shapeRenderer.line(lastX, lastY + 2f, currentX, currentY + 2f, Color.GREEN, Color.GREEN);
//            lastX = currentX;
//            lastY = currentY;
//            if (currentX > Main.WIDTH)
//                break;
//        }

        float xOffsetConst = 0.02f;
        currentXpos += xOffsetConst;

        float[] cl = carController.getCurvedLine();

        int startX = (int) (currentXpos / PerlinNoiseController.DOT_DISTANCE);
        float startXOffset = ((currentXpos / PerlinNoiseController.DOT_DISTANCE) - startX) / 1f * PerlinNoiseController.DOT_DISTANCE;

        float currX = 0f;
        float lastX = 0f;
        float yCenter = Main.HEIGHT / 2f;
        float lastY = startX > 0 ? cl[startX - 1] * 8f + yCenter : Main.HEIGHT / 2f;



        for (int currentIdx = startX; true; currentIdx++) {
            if (currX > Main.WIDTH - 0.6f)
                break;
            currX += PerlinNoiseController.DOT_DISTANCE;
            float currY = cl[currentIdx] * 8f + yCenter;
            shapeRenderer.line(lastX + 0.3f - startXOffset, lastY, currX + 0.3f - startXOffset, currY, Color.GREEN, Color.GREEN);
            lastX = currX;
            lastY = currY;

        }


        shapeRenderer.end();

    }

    @Override
    protected void renderSpritesInternal(float delta) {

    }


    @Override
    public void dispose() {
        shapeRenderer.dispose();
    }
}
