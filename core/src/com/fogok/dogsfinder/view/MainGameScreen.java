package com.fogok.dogsfinder.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;
import com.fogok.dogsfinder.Main;
import com.fogok.dogsfinder.overridesceneui.SuperImage;
import com.fogok.dogsfinder.overridesceneui.SuperImageButton;
import com.fogok.dogsfinder.overridesceneui.SuperLabel;
import com.fogok.dogsfinder.utils.TGen;
import com.fogok.dogsfinder.utils.guiwidgets.ARHelper;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.alpha;
import static com.fogok.dogsfinder.Main.DEBUG_MODE;

public class MainGameScreen extends BaseScreen{

    private Dog dog;

    public MainGameScreen(Main main) {
        super(main);
        renderSprites = true;
        renderStage = true;

        //main stage
        dog = new Dog(tGen);

        initBack();
        initTopBar();
        initBottomButtons();
    }

    //region Inits

    private void initBottomButtons() {
        final float padButtons = 0.1f;
        final float sizeButtons = 3f;
        final Container<SuperImageButton> dogClothesBtn = new Container<SuperImageButton>(
                new SuperImageButton(
                        new TextureRegionDrawable(tGen.getTR(8, TGen.AtlasType.MISC)),
                        new TextureRegionDrawable(tGen.getTR(9, TGen.AtlasType.MISC)),
                        new TextureRegionDrawable(tGen.getTR(11, TGen.AtlasType.MISC)))
        );
        dogClothesBtn.setFillParent(true);
        dogClothesBtn
                .width(ARHelper.getRealWidth(sizeButtons))
                .height(ARHelper.getRealWidth(sizeButtons))
                .align(Align.bottomLeft)
                .padLeft(ARHelper.getRealWidth(padButtons)).padBottom(ARHelper.getRealHeight(padButtons));
        dogClothesBtn.getActor().addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                SuperImageButton btn = (SuperImageButton)actor;
                switchTopBar(btn.isChecked());
            }
        });
        stage.addActor(dogClothesBtn);

        final Container<SuperImageButton> carStartGameBtn = new Container<SuperImageButton>(
                new SuperImageButton(
                        new TextureRegionDrawable(tGen.getTR(8, TGen.AtlasType.MISC)),
                        new TextureRegionDrawable(tGen.getTR(9, TGen.AtlasType.MISC)),
                        new TextureRegionDrawable(tGen.getTR(10, TGen.AtlasType.MISC))));
        carStartGameBtn.setFillParent(true);
        carStartGameBtn
                .width(ARHelper.getRealWidth(sizeButtons))
                .height(ARHelper.getRealWidth(sizeButtons))
                .align(Align.bottomRight)
                .padRight(ARHelper.getRealWidth(padButtons)).padBottom(ARHelper.getRealHeight(padButtons));
        carStartGameBtn.getActor().addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                main.getNativeController().signInToGoogle();
            }
        });

        stage.addActor(carStartGameBtn);
    }

    private Table topBarTable;
    private void initTopBar() {
        topBarTable = new Table();
        topBarTable.setTransform(true);
        fillTopBar(topBarTable);
        topBarTable.pack();
        topBarTable.setPosition(Gdx.graphics.getWidth() / 2f, Gdx.graphics.getHeight(), Align.top);
        topBarTable.setOrigin(Align.top);
        stage.addActor(topBarTable);
    }

    private void initBack() {
        Image background = new Image(tGen.getTR(0, TGen.AtlasType.BACKS));
        ARHelper.setWidgetSize(background, Main.HEIGHT, false);
        background.setX(ARHelper.getRealWidth(-1.2f));
        if (!DEBUG_MODE) {
            //animate
            background.getColor().a = 0f;
            background.addAction(alpha(1f, 0.5f));
        }
        stage.addActor(background);
    }

    //endregion

    //region Initialize TopBar

    private void fillTopBar(Table table){
        final float offsetTop = 0.1f;
        final float offsetTop2 = 0.1f;
        final float offsetBetween = 0.3f;
        final float widgSize = 1.8f;
        final float currencySize = 0.6f;

        addCurrencyImage(4, table, widgSize, offsetTop, offsetBetween);
        addCurrencyImage(5, table, widgSize, offsetTop, offsetBetween);
        addCurrencyImage(6, table, widgSize, offsetTop, offsetBetween);

        table.row();

        addCurrencyLabel(table, currencySize, 3, offsetTop2);
        addCurrencyLabel(table, currencySize, 3, offsetTop2);
        addCurrencyLabel(table, currencySize, 7, offsetTop2);


    }

    private void addCurrencyImage(int image, Table table, float widgSize, float offsetTop, float offsetBetween){
        Image bg = new Image(tGen.getTR(image, TGen.AtlasType.MISC));
        bg.setScaling(Scaling.fit);
        ARHelper.addToTableWithCustomSize(table, bg, widgSize, true)
                .padTop(ARHelper.getRealHeight(offsetTop))
                .padLeft(ARHelper.getRealHeight(offsetBetween))
                .padRight(ARHelper.getRealHeight(offsetBetween));
    }

    private void addCurrencyLabel(Table table, float size, int image, float offsetTop2){
        final Container<SuperLabel> label = new Container<SuperLabel>(new SuperLabel("12.3K", Color.BLACK));
        label.getActor().setFontScale(ARHelper.getRealHeight(0.013f));
        Stack stack = new Stack(
                new SuperImage(tGen.getTR(image, TGen.AtlasType.MISC)),
                label.align(Align.center).padRight(image == 7 ? ARHelper.getRealWidth(3.12f * size * 0.32f) : 0)
        );
        stack.pack();
        ARHelper.addToTableWithCustomSize(table, stack, size, false).padTop(ARHelper.getRealHeight(offsetTop2));
    }

    //endregion

    //region ClothesLogic

    private void switchTopBar(boolean isChecked){
//        System.out.println(topBarTable.getY() + " " + topBarTable.getHeight());
        if (topBarTable.getActions().size == 0) {

            final float scaleTo = isChecked ? 0.7f : 1f;
            topBarTable.addAction(Actions.scaleTo(scaleTo, scaleTo, 1f, !isChecked ? Interpolation.exp10In : Interpolation.exp10In));
        }
    }

    //endregion

    @Override
    protected void renderSpritesInternal(float delta) {
        dog.draw(batch);

        if (Gdx.input.isKeyJustPressed(Input.Keys.I))
            for (Actor actor : stage.getActors())
                System.out.println(actor.toString() + "\n");
        if (Gdx.input.isKeyJustPressed(Input.Keys.L))
            stage.setDebugAll(!stage.isDebugAll());


    }

    @Override
    public void dispose() {
        dog.dispose();
    }



//        stage.setDebugAll(true);
//      font tests:
//        final Table table = new Table();
//        table.setFillParent(true);
//
//        stage.setDebugAll(true);


//        UI.getDynamicFont().setScale(0.02f, 4);
//        final SuperLabel label = new SuperLabel("Привет \nHello World!\n123456789", Color.ORANGE);
//
////        label.setScale(UI.CONVERT_TO_ABSTRACT);
////        final Group group = new Group();
////        group.addActor(label);
////        group.setScale(UI.CONVERT_TO_ABSTRACT);
//
//        stage.addActor(label);
//
//        DebugValueChanger.setFloatObj(0, 1, new DebugValueChanger.DebugChangeListener() {
//            @Override
//            public void change(float change) {
////                stage.getActors().clear();
////                final Label label = new Label("Привет \nHello World!\n123456789", new Label.LabelStyle(UI.getDynamicFont(), Color.ORANGE));
//
////                label.setFontScale(UI.CONVERT_TO_ABSTRACT * change);
////                label.act(0.3f);
////                label.setPosition(3f, 3f);
////                stage.addActor(label);
//
//            }
//        });
}
