package com.fogok.dogsfinder.view;

import com.badlogic.gdx.Gdx;
import com.fogok.dogsfinder.Main;
import com.fogok.dogsfinder.utils.StartLogoAnim;

public class IntroScreen extends BaseScreen {

    private StartLogoAnim startLogoAnim;

    public IntroScreen(Main main) {
        super(main);
        startLogoAnim = new StartLogoAnim(tGen);
        startLogoAnim.setCompleteStartLogo(Main.DEBUG_MODE);
    }


    @Override
    protected void renderSpritesInternal(float delta) {
        if (!startLogoAnim.isCompleteStartLogo()){
            startLogoAnim.draw(batch);
            if (Gdx.input.justTouched())
                startLogoAnim.setCompleteStartLogo(true);
        }
        else
            main.setScreen(Main.Screen.MainGame);
    }

    @Override
    public void dispose() {

    }
}
