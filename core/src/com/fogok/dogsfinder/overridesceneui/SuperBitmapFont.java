package com.fogok.dogsfinder.overridesceneui;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;

import java.math.BigDecimal;


public class SuperBitmapFont extends BitmapFont {

    public SuperBitmapFont(FileHandle fontFile) {
        super(fontFile);
    }

    public SuperBitmapFont(BitmapFontData fontData, Array<TextureRegion> texRegions, boolean b) {
        super(fontData, texRegions, b);
    }

    public void setScale(float scaleXY, int decimalPlace){
        getData().setScale(new BigDecimal(scaleXY).setScale(decimalPlace, BigDecimal.ROUND_CEILING).floatValue());
    }

    public void setScale(float scaleXY){
        getData().setScale(scaleXY);
    }
}
