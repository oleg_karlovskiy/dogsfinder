package com.fogok.dogsfinder.overridesceneui;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

public class SuperImage extends Image {

    public SuperImage(TextureRegion region) {
        super(region);
    }

}
