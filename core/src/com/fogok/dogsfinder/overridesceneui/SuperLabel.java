package com.fogok.dogsfinder.overridesceneui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.fogok.dogsfinder.utils.ShaderBatch;
import com.fogok.dogsfinder.utils.UI;

public class SuperLabel extends Label {

    public SuperLabel(CharSequence text, Color color) {
        super(text, new LabelStyle(UI.getDynamicFont(), color));
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.setShader(UI.getFontShader());
        super.draw(batch, parentAlpha);
        ((ShaderBatch)batch).setDefaultShader();
    }
}
