package com.fogok.dogsfinder.overridesceneui;

import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class SuperImageButton extends ImageButton{
    public SuperImageButton(TextureRegionDrawable upDraw, TextureRegionDrawable downDraw, TextureRegionDrawable imageDraw) {
        super(new ImageButtonStyle(upDraw, downDraw, null, imageDraw, null, null));
        getStyle().pressedOffsetX = 2f;
        getStyle().pressedOffsetY = -2f;
    }

}
