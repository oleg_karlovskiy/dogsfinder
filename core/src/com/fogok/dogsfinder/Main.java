package com.fogok.dogsfinder;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.ScalingViewport;
import com.fogok.dogsfinder.utils.GMUtils;
import com.fogok.dogsfinder.utils.ShaderBatch;
import com.fogok.dogsfinder.utils.TGen;
import com.fogok.dogsfinder.utils.UI;
import com.fogok.dogsfinder.utils.debug.DebugValueChanger;
import com.fogok.dogsfinder.utils.gameres.Prefers;
import com.fogok.dogsfinder.view.BaseScreen;
import com.fogok.dogsfinder.view.CarGameScreen;
import com.fogok.dogsfinder.view.IntroScreen;
import com.fogok.dogsfinder.view.MainGameScreen;

import java.util.Collection;
import java.util.HashMap;

public class Main extends Game {

    public static float WIDTH, HEIGHT, mdT, dgnlCff;

    public static boolean DEBUG_MODE = true;
    public static boolean DEBUG_VALUECHANGER = false;
    public static String DEBUG_VALUE1 = "";
    public static String DEBUG_VALUE2 = "";

    private OrthographicCamera camera;
    public OrthographicCamera getCamera() {
        return camera;
    }

    private ShaderBatch batch;
    public ShaderBatch getBatch() {
        return batch;
    }

    private Stage stage;
    public Stage getStage() {
        return stage;
    }

    private TGen tGen;
    public TGen gettGen() {
        return tGen;
    }

    private NativeController nativeController;
    public NativeController getNativeController() {
        return nativeController;
    }

    //game
    public enum Screen{
        Intro, MainGame, CarGame
    }
    private HashMap<Screen, BaseScreen> screens = new HashMap<Screen, BaseScreen>();

    private void initScreens(){
        screens.put(Screen.Intro, new IntroScreen(this));
        screens.put(Screen.MainGame, new MainGameScreen(this));
        screens.put(Screen.CarGame, new CarGameScreen(this));

        setScreen(Screen.CarGame);
    }
    public void setScreen(Screen screen){
        setScreen(screens.get(screen));
    }
    //

    public Main(NativeController nativeController) {
        this.nativeController = nativeController;
    }

    @Override
	public void create () {
        startInit();
	}

	@Override
	public void render () {
        super.render();
        debugMethod0();
        mdT = Math.min(Gdx.graphics.getDeltaTime() / 0.016f /*/ slowHandler*/, 1.5f);
	}


    private void startInit(){
        batch = new ShaderBatch();

        //camera
        float aspectR = (float) Gdx.graphics.getWidth() / (float) Gdx.graphics.getHeight();
        camera = new OrthographicCamera(20f * aspectR, 20f);
        camera.position.set(new Vector3(20f * aspectR / 2f, 10f, 0f));
        WIDTH = camera.viewportWidth;
        HEIGHT = camera.viewportHeight;
        GMUtils.getDisplayBounds().set(0f, 0f, WIDTH, HEIGHT);

        //prefers
        resetPrefer();

        //debug
        debugBatch = new ShaderBatch();
        debugFont = new BitmapFont();
        Prefers.putInt(Prefers.KeyAppLaunchs, Prefers.getInt(Prefers.KeyAppLaunchs) + 1);
        launchNumber = "Launch number " + Prefers.getInt(Prefers.KeyAppLaunchs) + "`s";


        //main game
        stage = new Stage(new ScalingViewport(Scaling.stretch,
                Gdx.graphics.getWidth(),
                Gdx.graphics.getHeight(), new OrthographicCamera()), batch);

        UI.initializate();
        tGen = new TGen();

        InputMultiplexer inputMultiplexer = new InputMultiplexer();
        inputMultiplexer.addProcessor(stage);
        if (DEBUG_VALUECHANGER){
            debugValueChanger = new DebugValueChanger(2, debugBatch);
            inputMultiplexer.addProcessor(debugValueChanger.stage);
        }

        Gdx.input.setInputProcessor(inputMultiplexer);

        initScreens();
    }



    private void resetPrefer(){
        Prefers.initPrefs();
//        Prefers.putBool(Prefers.IsFirstStart, false);

        if (!Prefers.getBool(Prefers.IsFirstStart, false)){
            Prefers.putBool(Prefers.IsFirstStart, true);
        }
    }

    //region Debug
    private boolean textShow = false;
    private SpriteBatch debugBatch;
    private BitmapFont debugFont;
    private DebugValueChanger debugValueChanger;
    private String launchNumber;
    private void debugMethod0(){
        if (DEBUG_MODE && textShow)
            debugMethod1();

        if (debugValueChanger != null)
            debugMethod2();

        if (Gdx.input.isKeyJustPressed(Input.Keys.T))
            textShow = !textShow;
    }


    private float fpsIt;
    private int fps;
    private void debugMethod1(){
        debugBatch.begin();

        fpsIt += 1f * mdT;
        float fpsMax = 10f;
        if (fpsIt > fpsMax){
            fpsIt = 0f;
            fps = (int) (1f / Gdx.graphics.getDeltaTime());
        }
        debugFont.draw(debugBatch, "Reporting\n-------------\n".concat(launchNumber).concat("\n-------------\n").concat(
                "FPS:").concat(String.valueOf(fps)).concat("\n").concat(
                DEBUG_VALUE1).concat("\n").concat(
                DEBUG_VALUE2),
                10, 270);

        debugBatch.end();
    }
    private void debugMethod2(){
        debugValueChanger.draw();
    }
    //endregion

	@Override
	public void dispose () {
		stage.dispose();
        debugFont.dispose();
        debugBatch.dispose();
        UI.dispose();
        Collection<BaseScreen> screens = this.screens.values();
        for (BaseScreen screen : screens)
            screen.dispose();
    }
}
